# NF26 ― P2021 ― Projet

Les sujets et consignes sont consultables en ligne ici:

https://gitlab.utc.fr/NF26/tf-managed/TDs-p2021/nf26-projects/-/blob/main/README.md

## Structuration du dépôt

Ce dépôt reprend une structure minimale de projet, avec:

 - un dossier `project` pour votre code.
 - un `Pipfile` et son `Pipfile.lock`, contenant une liste des dépendances pour créer un environnement virtuel grâce à `make init`.
 - un `Makefile` simple avec quelques cibles pour le style de votre code.
 - un setup de `pre-commit` simple (`.pre-commit-config.yaml`).

Vous pouvez adapter ces fichiers à votre guise pour votre rendu.

## Documentation pour reproductibilité de votre projet

Afin de rendre vos travaux reproductibles, indiquez dans ce `README` les différentes
étapes pour obtenir vos résultats.

Vous pouvez compléter et adapter cet exemple:

 - Se connecter au serveur `nf26-1.leger.tf`.
 - Se rendre dans le dossier `/home/login/td-nf26-project-login`.
 - Lancer `make init` puis `make run`, etc.
