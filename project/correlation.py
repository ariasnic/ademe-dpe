import pyspark
import seaborn as sns
from pyspark.sql import SparkSession

def get_correlation_estimation_ges():

    conf = (
        pyspark.SparkConf()
        .setMaster("local[*]")
        .setAll(
            [
                ("spark.executor.memory", "1g"),  # find
                ("spark.driver.memory", "1g"),  # your
                ("spark.driver.maxResultSize", "1g"),  # setup
            ]
        )
    )
    # create the session
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # create the context
    df = spark.read.csv("/home/ariasnic/data/raw2", header=True)

    df.createOrReplaceTempView("ademe_33")

    corr1 = spark.sql(
        """
        SELECT
            corr(consommation_energie,estimation_ges)
            AS Correlation_Conso_Estimation_GES
        FROM ademe_33
    """
    )
    corr1.persist()
    corr1 = corr1.toPandas()
    return corr1


if __name__ == "__main__":
    print(get_correlation_estimation_ges())
