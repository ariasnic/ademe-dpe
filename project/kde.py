import csv

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import pandas as pd
import pyspark
from pyproj import Transformer
from pyspark.sql import SparkSession
from shapely.geometry import Point


def proj_streaming():
    with open("/home/ariasnic/data/raw", "r") as csvfile:
        with open("/home/ariasnic/data/raw2", "w", newline="") as csvfileW:
            reader = csv.reader(csvfile)
            transformer = Transformer.from_crs("epsg:4326", "epsg:3035")
            for i, row in enumerate(reader):
                if i == 0:
                    # Skipping headers
                    writer = csv.writer(csvfileW)
                    writer.writerow(row)
                    continue
                if (row[70] != "") & (row[69] != ""):
                    coords = transformer.transform(row[70], row[69])
                    row[70] = coords[0]
                    row[69] = coords[1]
                    writer = csv.writer(csvfileW)
                writer.writerow(row)


def kde():
    conf = (
        pyspark.SparkConf()
        .setMaster("local[*]")
        .setAll(
            [
                ("spark.executor.memory", "1g"),  # find
                ("spark.driver.memory", "1g"),  # your
                ("spark.driver.maxResultSize", "1g"),  # setup
            ]
        )
    )
    # create the session
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # create the context
    sc = spark.sparkContext
    spark

    # import data
    df = spark.read.csv("/home/ariasnic/data/raw2", header=True)

    df.createOrReplaceTempView("ademe_33")

    # Housing coordinates
    results = spark.sql(
        """
    SELECT
        longitude, latitude
    FROM ademe_33
    WHERE longitude IS NOT NULL
    AND latitude IS NOT NULL
    """
    )
    results.persist()

    # Grid creation
    minMax = spark.sql(
        """
    SELECT
        MIN(longitude) as min_longitude, MAX(longitude) as max_longitude,
        MIN(latitude) as min_latitude, MAX(latitude) as max_latitude
    FROM ademe_33
    WHERE longitude IS NOT NULL
    AND latitude IS NOT NULL
    """
    )
    print("Coordinates min and max : ")
    print(minMax)

    # Min & max are abberant, we chose ourself the grid delimitation

    transformer = Transformer.from_crs("epsg:4326", "epsg:3035")
    # Create corners of rectangle to be transformed to a grid
    sw = Point((44.224840, -1.291530))
    ne = Point((45.579593, 0.287861))

    # 10 km grid step size
    stepsize = 10000

    # Project corners to target projection
    transformed_sw = transformer.transform(sw.x, sw.y)
    transformed_ne = transformer.transform(ne.x, ne.y)

    # Iterate over 2D area
    gridpoints = []
    x = transformed_sw[0]
    while x < transformed_ne[0]:
        y = transformed_sw[1]
        while y < transformed_ne[1]:
            p = (x, y)
            gridpoints.append(p)
            y += stepsize
        x += stepsize
    len(gridpoints)

    # test of distance method, with Bordeaux Libourne
    transformer = Transformer.from_crs("epsg:4326", "epsg:3035")
    coords = (44.8333, -0.5667)
    coords = transformer.transform(coords[0], coords[1])
    row = (44.9167, -0.2333)
    row = transformer.transform(row[0], row[1])
    distance = Point(coords[0], coords[1]).distance(Point(row[0], row[1])) / 1000

    print("Distance Bordeaux Libourne :")
    print(distance)

    # neighborhood calculation
    voisinage = sc.parallelize([])
    for coords in gridpoints:
        new_voisinage = results.rdd.map(
            lambda row: (
                Point(coords[0], coords[1]),
                Point(float(row[1]), float(row[0])),
                Point(coords[0], coords[1]).distance(
                    Point(float(row[1]), float(row[0]))
                )
                / 1000,
            )
        ).filter(lambda row: row[2] < 15)
        voisinage = voisinage.union(new_voisinage)

    # sum of housing in each neighborhood
    voisinage1 = (
        voisinage.map(lambda x: ((x[0].x, x[0].y), 1))
        .reduceByKey(lambda a, b: a + b)
        .sortBy(lambda nb_logement: nb_logement[1], ascending=False)
    )

    # Vectors creation for plotting
    voisinage_pd = voisinage1.toDF().toPandas()
    z = voisinage_pd["_2"]
    xy = pd.DataFrame(voisinage_pd["_1"].tolist(), index=voisinage_pd.index)
    x = xy[0]
    y = xy[1]

    # Projection to GPS coordinates
    transformer = Transformer.from_crs("epsg:3035", "epsg:4326")
    x1 = []
    y1 = []
    for i, X in enumerate(x):
        for j, Y in enumerate(y):
            if i == j:
                pt = transformer.transform(X, Y)
                x1.append(pt[0])
                y1.append(pt[1])
    x1 = pd.DataFrame(x1)[0]
    y1 = pd.DataFrame(y1)[0]

    x2 = x1.to_numpy()
    y2 = y1.to_numpy()

    fig = plt.figure(figsize=(7, 7))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())

    ax.set_extent([0.28, -1.29, 45.57, 44.22], crs=ccrs.PlateCarree())

    ax.add_feature(cfeature.OCEAN.with_scale("50m"))
    ax.add_feature(cfeature.COASTLINE.with_scale("50m"))
    ax.add_feature(cfeature.RIVERS.with_scale("50m"), zorder=0)
    ax.add_feature(cfeature.BORDERS.with_scale("50m"), linestyle=":")
    ax.scatter(y2, x2, s=z / 100, color="black", transform=ccrs.PlateCarree())

    ax.set_title("Gironde")
    plt.savefig("density_estimation.png")


if __name__ == "__main__":
    proj_streaming()
    kde()
