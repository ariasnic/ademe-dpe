import pyspark
from pyspark.sql import SparkSession


def taux_A_by_city():
    conf = (
        pyspark.SparkConf()
        .setMaster("local[*]")
        .setAll(
            [
                ("spark.executor.memory", "1g"),  # find
                ("spark.driver.memory", "1g"),  # your
                ("spark.driver.maxResultSize", "1g"),  # setup
            ]
        )
    )
    # create the session
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # import data
    df = spark.read.csv("/home/ariasnic/data/raw2", header=True)

    df.createOrReplaceTempView("ademe_33")

    # Number of houses by city
    communeTotal = spark.sql(
        """
    SELECT
        commune, COUNT(*) as nb_logements
    FROM ademe_33
    GROUP BY commune
    ORDER BY nb_logements DESC
    """
    )
    communeTotal.persist()
    communeTotal.createOrReplaceTempView("communeTotal")

    # Ratio of houses A rated by city
    communeMostA = spark.sql(
        """
    SELECT
        ademe_33.commune, (COUNT(ademe_33.id)/communeTotal.nb_logements) as taux_A,
        COUNT(ademe_33.id), communeTotal.nb_logements
    FROM ademe_33 INNER JOIN communeTotal ON ademe_33.commune = communeTotal.commune
    WHERE ademe_33.classe_consommation_energie='A'
    AND communeTotal.nb_logements>500
    GROUP BY ademe_33.commune, communeTotal.nb_logements
    ORDER BY taux_A DESC
    """
    )
    communeMostA.persist()

    communeMostA = communeMostA.rdd.take(20)

    return communeMostA


def taux_A_by_housing_type():
    conf = (
        pyspark.SparkConf()
        .setMaster("local[*]")
        .setAll(
            [
                ("spark.executor.memory", "1g"),  # find
                ("spark.driver.memory", "1g"),  # your
                ("spark.driver.maxResultSize", "1g"),  # setup
            ]
        )
    )
    # create the session
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # import data
    df = spark.read.csv("/home/ariasnic/data/raw2", header=True)

    df.createOrReplaceTempView("ademe_33")

    # number of A rated housing by type of housing
    typeBattimentA = spark.sql(
        """
    SELECT
        tr002_type_batiment_libelle, COUNT(*) as nb_logements_A
    FROM ademe_33
    WHERE classe_consommation_energie='A'
    GROUP BY tr002_type_batiment_libelle
    ORDER BY nb_logements_A DESC
    """
    )
    typeBattimentA.persist()

    # number of housing by type of housing
    typeBattiment = spark.sql(
        """
    SELECT
        tr002_type_batiment_libelle, COUNT(*) as nb_logements
    FROM ademe_33
    GROUP BY tr002_type_batiment_libelle
    ORDER BY nb_logements DESC
    """
    )
    typeBattiment.persist()

    typeBattimentA_pd = typeBattimentA.toPandas()
    typeBattiment_pd = typeBattiment.toPandas()

    # ratio A rated housing by housing type
    ratio_A_by_housing_type = (
        typeBattimentA_pd["nb_logements_A"] / typeBattiment_pd["nb_logements"]
    )

    return ratio_A_by_housing_type


if __name__ == "__main__":
    print("Taux de batiments notés A pour la consommation énergétique, par commune : ")
    print(taux_A_by_city())
    print("-----------------")
    print("-----------------")
    print("Taux de batiments notés A par type de batiment : ")
    print("0 : Appartement,  1 : Maison, 2 : Logements collectifs")
    print(taux_A_by_housing_type())
