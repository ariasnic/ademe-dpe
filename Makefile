SHELL = /bin/bash

.DEFAULT_GOAL := all

## help: Display list of commands
.PHONY: help
help: Makefile
	@sed -n 's|^##||p' $< | column -t -s ':' | sed -e 's|^| |'

## all: Run all targets
.PHONY: all
all: init style run

## init: Bootstrap your application.
.PHONY: init
init:
	pre-commit install -t pre-commit
	pipenv install --dev

## format: Format code.
## style: Check lint, code styling rules.
.PHONY: format
.PHONY: style
style format:
	pre-commit run -a

## run: Run the project
.PHONY: run
run:
	PYTHONPATH=. pipenv run python project

## clean: Remove temporary files
.PHONY: clean
clean:
	-pipenv --rm

# correlation: Run the calculation of correlations
.PHONY: correlation
correlation:
	PYTHONPATH=. pipenv run python project/correlation.py

# classeA: Display data about A rated housing
.PHONY: classeA
classeA:
	PYTHONPATH=. pipenv run python project/classeA.py

# kde: Run kde and save plot
.PHONY: kde
kde:
	PYTHONPATH=. pipenv run python project/kde.py
